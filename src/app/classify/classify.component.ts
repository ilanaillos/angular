import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  favoriteSeason: string;
  selectedNetwork;
  networks: string[] = ['BBC', 'CNN', 'HBO', 'NBC'];
  
  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.selectedNetwork = this.route.snapshot.params.network; 
  }

}
