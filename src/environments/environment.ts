// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAL6kx8JT5aT1ck5KRd36uJfL5vFHAMJUg",
    authDomain: "hello-c9dd9.firebaseapp.com",
    databaseURL: "https://hello-c9dd9.firebaseio.com",
    projectId: "hello-c9dd9",
    storageBucket: "hello-c9dd9.appspot.com",
    messagingSenderId: "194437786737",
    appId: "1:194437786737:web:07a6178275e44814e8bfd0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
